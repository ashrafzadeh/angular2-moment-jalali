import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';
import * as jmoment from 'moment-jalaali';

// under systemjs, moment is actually exported as the default export, so we account for that
const momentConstructor: (value?: any) => moment.Moment = (<any>jmoment).default || jmoment;

@Pipe({ name: 'amUtc' })
export class UtcPipe implements PipeTransform {
  transform(value: Date | moment.Moment | string | number): moment.Moment {
    return jmoment(value).utc();
  }
}
